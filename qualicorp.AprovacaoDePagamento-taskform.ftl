<script type='text/javascript'>
	function notEmpty(elem){
		if(elem.value.length == 0){
			return false;
		}
		return true;
	}

	function isNumeric(elem){
		var numericExpression = /^[0-9]+$/;
		if(elem.value.match(numericExpression)){
			return true;
		} else {
			return false;
		}
	}

	function isAlphabet(elem){
        var alphaExp = /^[a-zA-Z0-9\u00A1-\uFFFF\_ .-@]+$/;
        if(elem.value.match(alphaExp)){
            return true;
        } else {
            return false;
        }
    }

    function isAlphanumeric(elem){
        var alphaExp = /^[a-zA-Z0-9\u00A1-\uFFFF\_ .-@]+$/;
        if(elem.value.match(alphaExp) && !isNumeric(elem)){
            return true;
        } else {
            return false;
        }
    }

	function isFloat(elem){
   		if(elem.value.indexOf(".") < 0){
     		return false;
   		} else {
      		if(parseFloat(elem.value)) {
              return true;
          	} else {
              return false;
          	}
   		}
	}

	function taskFormValidator() {
		var i=0;
		var myInputs = new Array();
					myInputs[i] = document.getElementById("aprovado");
					i++;
					myInputs[i] = document.getElementById("idSolicitacao");
					i++;
					myInputs[i] = document.getElementById("alcada");
					i++;
					myInputs[i] = document.getElementById("solicitante");
					i++;
					myInputs[i] = document.getElementById("valor");
					i++;
					myInputs[i] = document.getElementById("solicitaaprovacao");
					i++;
					myInputs[i] = document.getElementById("emailaprovadores");
					i++;
					myInputs[i] = document.getElementById("emailsolicitante");
					i++;
					myInputs[i] = document.getElementById("emailconferente");
					i++;
					myInputs[i] = document.getElementById("from");
					i++;
					myInputs[i] = document.getElementById("body");
					i++;
					myInputs[i] = document.getElementById("assunto");
					i++;
					myInputs[i] = document.getElementById("bodyaprovado");
					i++;
					myInputs[i] = document.getElementById("bodyreprovado");
					i++;


		var j=0;
						if(notEmpty(myInputs[j]) && !isAlphanumeric(myInputs[j])) {
							alert("Please enter valid aprovado");
							myInputs[j].focus();
							return false;
						}
					j++;
						if(notEmpty(myInputs[j]) && !isNumeric(myInputs[j])) {
							alert("Please enter valid idSolicitacao");
							myInputs[j].focus();
							return false;
						}
					j++;
						if(notEmpty(myInputs[j]) && !isAlphanumeric(myInputs[j])) {
							alert("Please enter valid alcada");
							myInputs[j].focus();
							return false;
						}
					j++;
						if(notEmpty(myInputs[j]) && !isAlphanumeric(myInputs[j])) {
							alert("Please enter valid solicitante");
							myInputs[j].focus();
							return false;
						}
					j++;
						if(notEmpty(myInputs[j]) && !isAlphanumeric(myInputs[j])) {
							alert("Please enter valid valor");
							myInputs[j].focus();
							return false;
						}
					j++;
						if(notEmpty(myInputs[j]) && !isAlphanumeric(myInputs[j])) {
							alert("Please enter valid solicitaaprovacao");
							myInputs[j].focus();
							return false;
						}
					j++;
						if(notEmpty(myInputs[j]) && !isAlphanumeric(myInputs[j])) {
							alert("Please enter valid emailaprovadores");
							myInputs[j].focus();
							return false;
						}
					j++;
						if(notEmpty(myInputs[j]) && !isAlphanumeric(myInputs[j])) {
							alert("Please enter valid emailsolicitante");
							myInputs[j].focus();
							return false;
						}
					j++;
						if(notEmpty(myInputs[j]) && !isAlphanumeric(myInputs[j])) {
							alert("Please enter valid emailconferente");
							myInputs[j].focus();
							return false;
						}
					j++;
						if(notEmpty(myInputs[j]) && !isAlphanumeric(myInputs[j])) {
							alert("Please enter valid from");
							myInputs[j].focus();
							return false;
						}
					j++;
						if(notEmpty(myInputs[j]) && !isAlphanumeric(myInputs[j])) {
							alert("Please enter valid body");
							myInputs[j].focus();
							return false;
						}
					j++;
						if(notEmpty(myInputs[j]) && !isAlphanumeric(myInputs[j])) {
							alert("Please enter valid assunto");
							myInputs[j].focus();
							return false;
						}
					j++;
						if(notEmpty(myInputs[j]) && !isAlphanumeric(myInputs[j])) {
							alert("Please enter valid bodyaprovado");
							myInputs[j].focus();
							return false;
						}
					j++;
						if(notEmpty(myInputs[j]) && !isAlphanumeric(myInputs[j])) {
							alert("Please enter valid bodyreprovado");
							myInputs[j].focus();
							return false;
						}
					j++;

		return true;
	}
</script>
<style type="text/css">
	#container
	{
		margin: 0 auto;
		width: 600px;
		background:#fff;
	}

	#header
	{
		background: #ccc;
		padding: 20px;
		font-family:Arial, Helvetica, sans-serif;
		font-size: 125%;
		letter-spacing: -1px;
		font-weight: bold;
		line-height: 1.1;
		color:#666;
	}

	#header h1 { margin: 0; }

	#content
	{
		clear: left;
		padding: 20px;
	}

	#content h2
	{
		color: #000;
		font-size: 160%;
		margin: 0 0 .5em;
	}

	#footer
	{
		background: #ccc;
		text-align: right;
		padding: 20px;
		height: 1%;
	}

	fieldset {
		border:1px dashed #CCC;
		padding:10px;
		margin-top:20px;
		margin-bottom:20px;
	}
	legend {
		font-family:Arial, Helvetica, sans-serif;
		font-size: 90%;
		letter-spacing: -1px;
		font-weight: bold;
		line-height: 1.1;
		color:#fff;
		background: #666;
		border: 1px solid #333;
		padding: 2px 6px;
	}
	.form {
		margin:0;
		padding:0;
	}
	label {
		width:140px;
		height:32px;
		margin-top:3px;
		margin-right:2px;
		padding-top:11px;
		padding-left:6px;
		background-color:#CCCCCC;
		float:left;
		display: block;
		font-family:Arial, Helvetica, sans-serif;
		font-size: 115%;
		letter-spacing: -1px;
		font-weight: normal;
		line-height: 1.1;
		color:#666;
	}
	.div_texbox {
		width:347px;
		float:right;
		background-color:#E6E6E6;
		height:35px;
		margin-top:3px;
		padding-top:5px;
		padding-bottom:3px;
		padding-left:5px;
	}
	.div_checkbox {
		width:347px;
		float:right;
		background-color:#E6E6E6;
		height:35px;
		margin-top:3px;
		padding-top:5px;
		padding-bottom:3px;
		padding-left:5px;
	}
	.textbox {
		background-color:#FFFFFF;
		background-repeat: no-repeat;
		background-position:left;
		width:285px;
		font:normal 18px Arial;
		color: #999999;
		padding:3px 5px 3px 19px;
	}
	.checkbox {
		background-color:#FFFFFF;
		background-repeat: no-repeat;
		background-position:left;
		width:285px;
		font:normal 18px Arial;
		color: #999999;
		padding:3px 5px 3px 19px;
	}
	.textbox:focus, .textbox:hover {
		background-color:#F0FFE6;
	}
	.button_div {
		width:287px;
		float:right;
		background-color:#fff;
		border:1px solid #ccc;
		text-align:right;
		height:35px;
		margin-top:3px;
		padding:5px 32px 3px;
	}
	.buttons {
		background: #e3e3db;
		font-size:12px; 
		color: #989070; 
		padding: 6px 14px;
		border-width: 2px;
		border-style: solid;
		border-color: #fff #d8d8d0 #d8d8d0 #fff;
		text-decoration: none;
		text-transform:uppercase;
		font-weight:bold;
	}
</style>
<div id="container">
	<div id="header">
		New Process Instance: /qualicorp/src/main/resources.AprovacaoDePagamento
	</div>
	<div id="content">
	    <input type="hidden" name="processId" value="${process.id}"/>
		<fieldset>
            <legend>Process inputs</legend>
                            		<label for="name">aprovado</label>
                            		<div class="div_checkbox">
                              		<input name="aprovado" type="checkbox" class="checkbox" id="aprovado" value="true" />
                            		</div>
              	
                            		<label for="name">idSolicitacao</label>
                            		<div class="div_texbox">
                              		<input name="idSolicitacao" type="text" class="textbox" id="idSolicitacao" value="" />
                            		</div>
              	
                            		<label for="name">alcada</label>
                            		<div class="div_checkbox">
                              		<input name="alcada" type="checkbox" class="checkbox" id="alcada" value="true" />
                            		</div>
              	
                            		<label for="name">solicitante</label>
                            		<div class="div_texbox">
                              		<input name="solicitante" type="text" class="textbox" id="solicitante" value="" />
                            		</div>
              	
                            		<label for="name">valor</label>
                            		<div class="div_texbox">
                              		<input name="valor" type="text" class="textbox" id="valor" value="" />
                            		</div>
              	
                            		<label for="name">solicitaaprovacao</label>
                            		<div class="div_checkbox">
                              		<input name="solicitaaprovacao" type="checkbox" class="checkbox" id="solicitaaprovacao" value="true" />
                            		</div>
              	
                            		<label for="name">emailaprovadores</label>
                            		<div class="div_texbox">
                              		<input name="emailaprovadores" type="text" class="textbox" id="emailaprovadores" value="" />
                            		</div>
              	
                            		<label for="name">emailsolicitante</label>
                            		<div class="div_texbox">
                              		<input name="emailsolicitante" type="text" class="textbox" id="emailsolicitante" value="" />
                            		</div>
              	
                            		<label for="name">emailconferente</label>
                            		<div class="div_texbox">
                              		<input name="emailconferente" type="text" class="textbox" id="emailconferente" value="" />
                            		</div>
              	
                            		<label for="name">from</label>
                            		<div class="div_texbox">
                              		<input name="from" type="text" class="textbox" id="from" value="" />
                            		</div>
              	
                            		<label for="name">body</label>
                            		<div class="div_texbox">
                              		<input name="body" type="text" class="textbox" id="body" value="" />
                            		</div>
              	
                            		<label for="name">assunto</label>
                            		<div class="div_texbox">
                              		<input name="assunto" type="text" class="textbox" id="assunto" value="" />
                            		</div>
              	
                            		<label for="name">bodyaprovado</label>
                            		<div class="div_texbox">
                              		<input name="bodyaprovado" type="text" class="textbox" id="bodyaprovado" value="" />
                            		</div>
              	
                            		<label for="name">bodyreprovado</label>
                            		<div class="div_texbox">
                              		<input name="bodyreprovado" type="text" class="textbox" id="bodyreprovado" value="" />
                            		</div>
              	

          </fieldset>
	</div>
	<div id="footer">
	</div>
</div>